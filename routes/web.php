<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/login', function () {
  return view('login');
});
Route::post('/login', 'UserController@login');
Route::post('/API_PT2_BOOKING', 'ODPController@API_PT2_BOOKING');
Route::post('/API_PT2_CEKSLOT', 'ODPController@API_PT2_CEKSLOT');
Route::get('/getOdcBySTO/{sto}', 'MitosController@getOdcBySTO');
Route::group(['middleware' => 'tomman.auth'], function () {
  Route::get('/logout', 'UserController@logout');
  Route::get('/', 'HomeController@home');
  
  Route::get('/rekap', 'HomeController@rekap');
  Route::get('/list/{step_id}', 'MitosController@list');
  Route::get('/listdata/{step_id}', 'MitosController@listdata');
  Route::get('/registerLop', 'MitosController@register_lop');
  Route::get('/progress/{id}', 'MitosController@progress');
  
  Route::post('/save_register_lop/{id}', 'MitosController@save_register_lop');
  Route::post('/save_aanwijzing/{id}', 'MitosController@save_aanwijzing');
  Route::post('/save_booking_odp/{id}', 'MitosController@save_booking_odp');
  Route::post('/save_pemberkasan/{id}', 'MitosController@save_pemberkasan');
  Route::post('/save_verifikasi_sdi/{id}', 'MitosController@save_verifikasi_sdi');
  Route::post('/save_golive/{id}', 'MitosController@save_golive');

  //mitra
  Route::get('/mitra', 'MitraController@index');
  Route::get('/mitra/{id}', 'MitraController@form');
  Route::post('/mitra/{id}', 'MitraController@save');
  //tematik
  Route::get('/tematik', 'TematikController@index');
  Route::get('/tematik/{id}', 'TematikController@form');
  Route::post('/tematik/{id}', 'TematikController@save');
  //master odc
  Route::get('/odc', 'ODCController@index');
  Route::get('/odc/{id}', 'ODCController@form');
  Route::post('/odc/{id}', 'ODCController@save');
  //master odp
  Route::get('/odp/{sto}/{odc}', 'ODPController@index');
  Route::get('/odp/{id}', 'ODPController@form');
  Route::post('/odp/{id}', 'ODPController@update');
  Route::post('/save_request_odp/{id}', 'ODPController@save_request_odp');
  Route::post('/save_request_odp_single/{id}', 'ODPController@save_request_odp_single');

  //master_odp
  Route::get('/master_odp', 'ODPController@master_odp');
  Route::get('/master_odp/get', 'ODPController@get_master_odp');

  //proaktif
  Route::get('/proaktif', 'ProaktifController@index');
  Route::get('/proaktif/{id}', 'ProaktifController@form');
  Route::post('/proaktif/{id}', 'MitosController@saveimport');

  Route::get('/umurodp','MatrikController@matrik1');
  Route::get('/umurodp/{mitra}/{l}','MatrikController@matrik1_detil');
});
