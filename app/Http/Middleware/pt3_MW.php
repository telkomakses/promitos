<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
class pt3_MW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::has('auth')){
            return redirect('login');
        }else{
            // if(Session::get('auth')->promise_level != 2){
            // }
        }
        return $next($request);
    }
}
