<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\DA\HomeModel;
use App\DA\LogModel;
use App\DA\MitosModel;
use App\DA\MasterODP;
use App\DA\MitraModel;
use App\DA\TematikModel;
use App\DA\UploadModel;
use App\DA\BoqModel;
use App\DA\MasterODC;

use Excel;

use Illuminate\Support\Facades\Session;

class MitosController extends Controller
{
    public function progress($id)
    {   
        $data = MitosModel::getById($id);
        // dd($data);
        switch ($data->step_id) {
        case 1:
            $mitra = MitraModel::getAktifAll();
            $tematik = TematikModel::getAktifAll();
            $sto = MasterODC::getAllSTO();
            return view('mitos.form.register_lop', compact('data', 'mitra', 'tematik', 'sto'));
            break;
        case 2:
            return view('mitos.form.aanwijzing', compact('data'));
            break;
        case 3:
            $odp=null;
            if (file_exists(public_path().'/storage/'.$id.'/reg_file/kml_plan.kml')) {
                $kml = simplexml_load_file(public_path().'/storage/'.$id.'/reg_file/kml_plan.kml');
                $odp=null;
                $isikml = json_decode(json_encode($kml->Document->Folder[0]),true);
                if(isset($isikml['Folder'])){
                    $odp = $isikml;
                }
            }
            $booked = MasterODP::getByIdLop($id);
            return view('mitos.form.booking_odp', compact('data','booked','odp'));
            break;
        case 4:
        $booked = MasterODP::getByIdLop($id);
            return view('mitos.form.pemberkasan', compact('data','booked'));
            break;
        case 5:
        $booked = MasterODP::getByIdLop($id);
            return view('mitos.form.verifikasi_sdi', compact('data','booked'));
            break;
        case 6:
        $booked = MasterODP::getByIdLop($id);
            return view('mitos.form.golive', compact('data','booked'));
            break;
        default:
            dd('wrong step, back and reload.');
            break;
        }
    }

    public function listdata(Request $req,$stepid)
    {   
        $draw = $req->get('draw');
        $start = $req->get("start");
        $rowperpage = $req->get("length"); // Rows display per page

        $columnIndex_arr = $req->get('order');
        $columnName_arr = $req->get('columns');
        $order_arr = $req->get('order');
        $search_arr = $req->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = MitosModel::countTotalRecord($stepid);
        $totalRecordswithFilter = MitosModel::countTotalRecordswithFilter($stepid,$searchValue);

        // Fetch records
        $records = MitosModel::getRecords($stepid,$start,$rowperpage,$columnName,$columnSortOrder,$searchValue);

        $data_arr = array();
        $sno = $start+1;
        foreach($records as $record){
            $data_arr[] = array(
                "id" => $record->id,
                "nama_lop" => $record->nama_lop,
                "jenis_pekerjaan" => $record->jenis_pekerjaan,
                "mitra" => $record->mitra,
                "totalterminal" => $record->totalterminal
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        ); 

        echo json_encode($response);
        exit;
        // return view('mitos.list',compact('data'));
    }
    public static function list($step){
        return view('mitos.list');
    }
    public function getOdcBySTO($sto){
        return json_encode(MasterODC::getAllODC($sto));
    }
    public function register_lop(){
        $data = new \stdClass();
        $mitra = MitraModel::getAktifAll();
        $tematik = TematikModel::getAktifAll();
        $sto = MasterODC::getAllSTO();
        return view('mitos.form.register_lop', compact('data', 'mitra', 'tematik', 'sto'));
    }
    public function saveimport(Request $req, $id)
      {
        $auth = session('auth');
        $param = [
            'proaktif_id'       => $req->proaktif_id,
            'step_id'       => 3,
            'nama_lop'      => $req->nama_lop,
            'jenis_pekerjaan'      => $req->jenis_pekerjaan,
            'sto'      => $req->sto,
            'odc'      => $req->odc,
            'mitra_id'      => $req->mitra_id,
            'tematik_id'      => $req->tematik_id,
            'tgl_create'      => DB::raw('now()')
        ];
        $id = MitosModel::insertMaster($param);
        $upload = [];
        foreach(json_decode($req->boq) as $b){
          $upload[] = [
            'designator' => $b->designator,
            'hargamaterial' => str_replace(',', '', $b->telkom_material),
            'hargajasa' => str_replace(',', '', $b->telkom_jasa),
            'qty' => str_replace(',', '', $b->volume), 
            'mitos_lop_id' => $id, 
            'status' => 1
          ];
        }
        BoqModel::insertboq($id,$upload);

        $this->handleFileUploads($req, ['kml_plan'], public_path().'/storage/'.$id.'/reg_file/');
        LogModel::insertLog(['mitos_lop_id'=>$id,'catatan'=>'','create_by'=>$auth->id_user,'step_id'=>1]);
        
        return redirect('/proaktif');
      }
    public function save_register_lop(Request $req, $id){
        
        //baca kmz
        // if ($req->hasFile('boq_plan')) {
        //     $zip = new \ZipArchive;
        //     $res = $zip->open($req->file('boq_plan'));
        //     if ($res === TRUE) {
        //       $zip->extractTo(public_path().'/upload/');
        //       $zip->close();
        //       $xml = simplexml_load_file(public_path().'/upload/doc.kml');
        //      $data = $xml->Document->Folder->Placemark;
        //      dd($xml,$data);
        //       echo 'Success!';
        //     } else {
        //       echo 'errors';
        //     }
        // }
        // dd('asd');

        $namalop = "$req->jenis_pekerjaan$req->tahap".substr($req->tahun, 2,2)."-$req->sto-$req->odc-$req->nama_lop";
        // dd($namalop);
        $auth = session('auth');
        $param = [
            'step_id'       => 3,
            'nama_lop'      => $namalop,
            'jenis_pekerjaan'      => $req->jenis_pekerjaan,
            'sto'      => $req->sto,
            'odc'      => $req->odc,
            'mitra_id'      => $req->mitra_id,
            'tematik_id'      => $req->tematik_id,
            'nilai_jasa'      => $req->nilai_jasa,
            'nilai_material'      => $req->nilai_material,
            'tgl_create'      => DB::raw('now()')
        ];
        if($id){
            //update
            MitosModel::updateMaster($id, $param);
        }else{
            //insert
            $id = MitosModel::insertMaster($param);
        }
        // if ($req->hasFile('boq_plan')) {
        //     $file = $req->file('boq_plan');
        //     $nama_file = $file->getClientOriginalName();
        //     $arr = Excel::toArray(new UploadModel, $file);
        //     $upload = [];
        //         foreach ($arr[0] as $no => $a) {
        //             if ($no >= 10 && $a[1] != '' && $a[6] != '' && substr($a[6], 0, 1) != '=') {
        //                 $upload[] = ['designator' => $a[1],'hargamaterial' => $a[4],'hargajasa' => $a[5], 'qty' => $a[6], 'mitos_lop_id' => $id, 'status' => 1];
        //             }
        //         }
        //         // dd($upload);
        //         BoqModel::insertboq($id,$upload);
        // }
        // 
        $this->handleFileUploads($req, ['boq_plan','kml_plan'], public_path().'/storage/'.$id.'/reg_file/');
        LogModel::insertLog(['mitos_lop_id'=>$id,'catatan'=>'','create_by'=>$auth->id_user,'step_id'=>1]);
        
        return redirect('/list/1');
    }
    public function save_aanwijzing(Request $req, $id){
        $auth = session('auth');
        $param = [
            'step_id'       => 3,
        ];
        MitosModel::updateMaster($id, $param);
        LogModel::insertLog(['mitos_lop_id'=>$id,'catatan'=>'','create_by'=>$auth->id_user,'step_id'=>2]);
        return redirect('/list/2');
    }
    public function save_booking_odp(Request $req, $id){
        $auth = session('auth');
        $param = [
            'step_id'       => 4,
        ];
        MitosModel::updateMaster($id, $param);
        LogModel::insertLog(['mitos_lop_id'=>$id,'catatan'=>'','create_by'=>$auth->id_user,'step_id'=>3]);
        return redirect('/list/3');
    }
    // public function save_request_odp(Request $req, $id){
    //     $array = json_decode($req->terminal);
    //     if($req->jumlah_terminal>1){
    //         usort($array,function($first,$second){
    //             return explode(' ', $first->name)[1] > explode(' ', $second->name)[1];
    //         });
    //     }else{
    //         $array = [json_decode($req->terminal)];
    //     }
    //     $free = MasterODP::getFreeRow($req);
    //     $lastIndex = MasterODP::getLastIndex($req->jenis_terminal,$req->sto,$req->odc);
    //     if(count($lastIndex)){
    //         $lastIndex = $lastIndex->index_terminal;
    //     }else{
    //         $lastIndex = '000';
    //     }
    //     $update = $insert = [];
    //     foreach($array as $no => $a){
    //         // dd($req->jumlah_terminal,$array,$a);
    //         if($no<count($free)){
    //             $update[] = ['update'=>['mitos_lop_id'=>$id,'status'=>'Booked','tgl_order'=>DB::raw('now()'),'koordinat_kml'=>$a->Point->coordinates],'id'=>$free[$no]->id];
    //             echo $a->name.'(U)'.$free[$no]->id.'<br>';
    //         }else{
    //             $lastIndex++;

    //             switch (strlen($lastIndex)) {
    //             case 1:
    //                 $lastIndex = '00'.$lastIndex;
    //                 break;
    //             case 2:
    //                 $lastIndex = '0'.$lastIndex;
    //                 break;
    //             case 3:
    //                 $lastIndex = $lastIndex;
    //                 break;
    //             default:
    //                 dd('index count off limit');
    //                 break;
    //             }
    //             // dd($lastIndex);
    //             echo $a->name.'('.$lastIndex.')<br>';
    //             $insert[] = [
    //                 'mitos_lop_id'          => $id,
    //                 'jenis_terminal'        => $req->jenis_terminal,
    //                 'sto'                   => $req->sto,
    //                 'odc'                   => $req->odc,
    //                 'index_terminal'        => $lastIndex,
    //                 'tgl_order'             => DB::raw('now()'),
    //                 'koordinat_kml'         => $a->Point->coordinates,
    //                 'status'                => 'Booked'
    //             ];
    //         }
    //     }

    //     $auth = session('auth');
    //     foreach($update as $u){
    //         MasterODP::updateFreeSingle($u);
    //     }
    //     MasterODP::insertNewSlot($insert);
    //     LogModel::insertLog(['mitos_lop_id'=>$id,'catatan'=>'','create_by'=>$auth->id_user,'step_id'=>3]);
    //     return redirect('/progress/'.$id);
    // }

    

    public function save_pemberkasan(Request $req, $id){
        $auth = session('auth');
        $param = [
            'step_id'       => 5,
        ];
        MitosModel::updateMaster($id, $param);

        $this->handleFileUploads($req, ['abd'], public_path().'/storage/'.$id.'/reg_file/');
        LogModel::insertLog(['mitos_lop_id'=>$id,'catatan'=>'','create_by'=>$auth->id_user,'step_id'=>4]);
        return redirect('/list/4');
    }
    public function save_verifikasi_sdi(Request $req, $id){
        $auth = session('auth');
        $param = [
            'step_id'       => $req->status,
        ];
        MitosModel::updateMaster($id, $param);
        LogModel::insertLog(['mitos_lop_id'=>$id,'catatan'=>$req->catatan,'create_by'=>$auth->id_user,'step_id'=>5]);
        return redirect('/list/5');
    }
    public function save_golive(Request $req, $id){
        $auth = session('auth');
        $param = [
            'step_id'       => 7,
        ];
        MitosModel::updateMaster($id, $param);
        LogModel::insertLog(['mitos_lop_id'=>$id,'catatan'=>'','create_by'=>$auth->id_user,'step_id'=>6]);
        return redirect('/list/6');
    }
    private function handleFileUploads($request, $file, $path)
    {
        foreach ($file as $name) {
            $input = $name;
            if ($request->hasFile($input)) {
                if (! file_exists($path)) {
                    if (! mkdir($path, 0770, true)) {
                        return redirect()->back()->with('alertblock', [
                            ['type' => 'danger', 'text' => 'Gagal menyiapkan folder'],
                        ]);
                    }
                }
                $file = $request->file($input);
                try {
                    $ext = $file->getClientOriginalExtension();
                    $moved = $file->move("$path", "$name.$ext");
                } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                    return redirect()->back()->with('alertblock', [
                        ['type' => 'success', 'text' => 'Gagal Upload'],
                    ]);
                }
            }
        }
    }
}