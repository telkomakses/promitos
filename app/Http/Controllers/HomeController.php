<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\HomeModel;
use App\DA\MitosModel;
use Excel;

use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    
    public function home()
    {   
        // $data = HomeModel::getData();
        // return view('home', compact('data'));
        return view('home');
    }
    public function rekap()
    {   
        return view('mitos.rekap');
    }
    public function rekapdata()
    {   
        $data = MitosModel::getAll();
        return view('mitos.rekap');
    }

}