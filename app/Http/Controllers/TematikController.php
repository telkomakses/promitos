<?php
namespace App\Http\Controllers;

use App\DA\TematikModel;
use Illuminate\Http\Request;

class TematikController extends Controller
{
  //tematik
  public function index()
  {
      $data = TematikModel::getAll();
      return view('tematik.index', compact('data'));
  }
  public function form($id)
  {
      $data = TematikModel::getById($id);
      return view('tematik.input', compact('data'));
  }
  public function save($id, Request $req)
  {
      TematikModel::insertOrUpdate($id, $req);
      return redirect('/tematik');
  }

}
