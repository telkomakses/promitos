<?php
namespace App\Http\Controllers;

use App\DA\MitraModel;
use Illuminate\Http\Request;

class MitraController extends Controller
{
  //mitra
  public function index()
  {
      $data = MitraModel::getAll();
      return view('mitra.index', compact('data'));
  }
  public function form($id)
  {
      $data = MitraModel::getById($id);
      return view('mitra.input', compact('data'));
  }
  public function save($id, Request $req)
  {
      MitraModel::insertOrUpdate($id, $req);
      return redirect('/mitra');
  }

}
