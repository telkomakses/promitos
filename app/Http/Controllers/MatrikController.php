<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\MatrikModel;
use Illuminate\Support\Facades\Session;
class MatrikController extends Controller
{
    public function matrik1()
    {   
        $data = MatrikModel::getMatrik1();
        return view('matrik.umur_booking', compact('data'));
    }
    public function matrik1_detil($mitra,$l)
    {   
        $data = MatrikModel::getMatrik1_detil($mitra,$l);
        return view('matrik.umur_booking_detil', compact('data'));
    }

}