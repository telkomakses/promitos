<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;
use App\DA\ApiModel;

class UserController extends Controller
{
	public function login(Request $request)
  {
    $username  = $request->input('username');
    $password = $request->input('password');

    $url = 'https://marina.bjm.tomman.app/TOMMANAUTH';
    $data = 'login=' . $username . '&password=' . $password; 
    $api = json_decode(ApiModel::post($url,$data));
    if ($api->data) {
      Session::put('auth', $api->data);
      return redirect($api->data->promise_level==1?'/':'/');  
    }else{
    	return redirect()->back()->with('alertblock', [
        ['type' => 'danger', 'text' => 'Gagal Login']
      ]);
    }
  }
  public function logout()
  {
    Session::forget('auth');
    return redirect('/login');
  }
  public function themes()
  {
    return view('theme');
  }
  public function themeSave(Request $req )
  {
    session('auth')->pixeltheme=$req->theme;
    DB::table('user')->where('id_user', session('auth')->id_user)->update([
      'pixeltheme'  => $req->theme
    ]);
    return back();
  }
}