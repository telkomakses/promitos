<?php
namespace App\Http\Controllers;

use App\DA\MasterODP;
use App\DA\MasterODC;
use App\DA\MitosModel;
use App\DA\LogModel;
use App\DA\FTPModel;

use DB;

use Illuminate\Http\Request;

class ODPController extends Controller
{
  public function index($sto,$odc)
  {
    $s_sto = MasterODC::getAllSTO();
    $s_odc = MasterODC::getAllODC($sto);
    $data = new \stdclass;
    if($odc){
      $data = MasterODP::getByODC($sto,$odc);
    }
      // dd($s_sto,$s_odc);
      return view('odp.index', compact('data', 's_sto', 's_odc'));
  }
  public function form($id)
  {
      $data = MasterODP::getById($id);
      return view('odp.input', compact('data'));
  }
  public function update($id, Request $req)
  {
    MasterODP::update($id, $req);
    return redirect("/odp/$req->sto/$req->odc");
  }
  public function API_PT2_BOOKING(Request $req){

    // return (json_decode($req->terminal)[0]->jenis_terminal);
    if($req->nama_lop && $req->jenis_pekerjaan && $req->sto && $req->odc && $req->terminal && $req->id_regu){
      $id = MitosModel::insertMaster([
        'step_id'           => 4,
        'nama_lop'          => $req->nama_lop,
        'jenis_pekerjaan'   => $req->jenis_pekerjaan,
        'tahun'             => date('Y'),
        'sto'               => $req->sto,
        'odc'               => $req->odc,
        'mitra_id'          => 1,
        'id_regu'           => $req->id_regu,
        'tematik_id'        => -1,
        'tgl_create'        => DB::raw('now()')
      ]);
      foreach(json_decode($req->terminal) as $t){
        $jt = $t->jenis_terminal;
        $free = MasterODP::getFreeRowSingle($jt,$req->sto,$req->odc,$req->id_regu);
        $lastIndex = MasterODP::getLastIndex($jt,$req->sto,$req->odc);
        if($lastIndex){
          $lastIndex = $lastIndex->index_terminal;
        }else{
          $lastIndex = 0;
        }
        $idbooking = 0;
        if(count($free)){
          MasterODP::updateFreeSingle(['update'=>['mitos_lop_id'=>$id,'status'=>'Booked','tgl_order'=>DB::raw('now()'),'koordinat_kml'=>$t->koordinat],'id'=>$free->id]);
          $idbooking = $free->id;
        }else{
          $lastIndex++;
          switch (strlen($lastIndex)) {
          case 1:
            $lastIndex = '00'.$lastIndex;
            break;
          case 2:
            $lastIndex = '0'.$lastIndex;
            break;
          case 3:
            $lastIndex = $lastIndex;
            break;
          default:
            dd('index count off limit');
            break;
          }
          $idbooking = MasterODP::insertNewSlotGetId([
            'mitos_lop_id'          => $id,
            'jenis_terminal'        => $jt,
            'sto'                   => $req->sto,
            'odc'                   => $req->odc,
            'index_terminal'        => $lastIndex,
            'tgl_order'             => DB::raw('now()'),
            'koordinat_kml'         => $t->koordinat,
            'status'                => 'Booked'
          ]);
        }
      }
      
      LogModel::insertLog(['mitos_lop_id'=>$id,'catatan'=>'','create_by'=>'TOMMAN_PT2','step_id'=>3]);
      return ['status'=>'success', 'booked'=>MasterODP::getByIdLop($id)];
    }else{
        return ['status'=>'error', 'booked'=>null];
    }
  }
  public function API_PT2_CEKSLOT(Request $req){
    if($req->jenis_terminal && $req->sto && $req->odc){
      return ['status'=>'success', 'data'=>MasterODP::getAllByOdc($req->jenis_terminal,$req->sto,$req->odc)];
    }else{
      return ['status'=>'error', 'data'=>null];
    }
  }
  public function save_request_odp(Request $req, $id){
    $array = json_decode($req->terminal);
    if($req->jumlah_terminal>1){
      usort($array,function($first,$second){
          return explode(' ', $first->name)[1] > explode(' ', $second->name)[1];
      });
    }else{
      $array = [json_decode($req->terminal)];
    }
    $free = MasterODP::getFreeRow($req);
    $lastIndex = MasterODP::getLastIndex($req->jenis_terminal,$req->sto,$req->odc);
    if(count($lastIndex)){
      $lastIndex = $lastIndex->index_terminal;
    }else{
      $lastIndex = '000';
    }
    $update = $insert = [];
    foreach($array as $no => $a){
      // dd($req->jumlah_terminal,$array,$a);
      if($no<count($free)){
        $update[] = ['update'=>['mitos_lop_id'=>$id,'status'=>'Booked','tgl_order'=>DB::raw('now()'),'koordinat_kml'=>$a->Point->coordinates],'id'=>$free[$no]->id];
        echo $a->name.'(U)'.$free[$no]->id.'<br>';
      }else{
        $lastIndex++;
        switch (strlen($lastIndex)) {
        case 1:
          $lastIndex = '00'.$lastIndex;
          break;
        case 2:
          $lastIndex = '0'.$lastIndex;
          break;
        case 3:
          $lastIndex = $lastIndex;
          break;
        default:
          dd('index count off limit');
          break;
        }
        // dd($lastIndex);
        echo $a->name.'('.$lastIndex.')<br>';
        $insert[] = [
          'mitos_lop_id'          => $id,
          'jenis_terminal'        => $req->jenis_terminal,
          'sto'                   => $req->sto,
          'odc'                   => $req->odc,
          'index_terminal'        => $lastIndex,
          'tgl_order'             => DB::raw('now()'),
          'koordinat_kml'         => $a->Point->coordinates,
          'status'                => 'Booked'
        ];
      }
    }

    $auth = session('auth');
    foreach($update as $u){
        MasterODP::updateFreeSingle($u);
    }
    MasterODP::insertNewSlot($insert);
    LogModel::insertLog(['mitos_lop_id'=>$id,'catatan'=>'','create_by'=>$auth->id_user,'step_id'=>3]);
    return redirect('/progress/'.$id);
  }
  public function save_request_odp_single(Request $req, $id){
    $free = MasterODP::getFreeRow($req);
    $koordinat = $req->long.','.$req->lat.',0';
    $lastIndex = MasterODP::getLastIndex($req->jenis_terminal,$req->sto,$req->odc);
    if(count($lastIndex)){
      $lastIndex = $lastIndex->index_terminal;
    }else{
      $lastIndex = '000';
    }
    if(count($free)){
      MasterODP::updateFreeSingle(['update'=>['mitos_lop_id'=>$id,'status'=>'Booked','tgl_order'=>DB::raw('now()'),'koordinat_kml'=>$koordinat],'id'=>$free[0]->id]);
    }else{
      $lastIndex++;
      switch (strlen($lastIndex)) {
      case 1:
        $lastIndex = '00'.$lastIndex;
        break;
      case 2:
        $lastIndex = '0'.$lastIndex;
        break;
      case 3:
        $lastIndex = $lastIndex;
        break;
      default:
        dd('index count off limit');
        break;
      }
      MasterODP::insertNewSlot([
        'mitos_lop_id'          => $id,
        'jenis_terminal'        => $req->jenis_terminal,
        'sto'                   => $req->sto,
        'odc'                   => $req->odc,
        'index_terminal'        => $lastIndex,
        'tgl_order'             => DB::raw('now()'),
        'koordinat_kml'         => $koordinat,
        'status'                => 'Booked'
      ]);
    }
    $auth = session('auth');
    LogModel::insertLog(['mitos_lop_id'=>$id,'catatan'=>'','create_by'=>$auth->id_user,'step_id'=>3]);
    return redirect('/progress/'.$id);
  }
  public static function master_odp(){
    return view('odp.ftp.index');
  }
  public static function get_master_odp(Request $req){
    $draw = $req->get('draw');
    $start = $req->get("start");
    $rowperpage = $req->get("length"); // Rows display per page

    $columnIndex_arr = $req->get('order');
    $columnName_arr = $req->get('columns');
    $order_arr = $req->get('order');
    $search_arr = $req->get('search');

    $columnIndex = $columnIndex_arr[0]['column']; // Column index
    $columnName = $columnName_arr[$columnIndex]['data']; // Column name
    $columnSortOrder = $order_arr[0]['dir']; // asc or desc
    $searchValue = $search_arr['value']; // Search value

    // Total records
    $totalRecords = FTPModel::countTotalRecord();
    $totalRecordswithFilter = FTPModel::countTotalRecordswithFilter($searchValue);

    // Fetch records
    $records = FTPModel::getRecords($start,$rowperpage,$columnName,$columnSortOrder,$searchValue);

    $data_arr = array();
    $sno = $start+1;
    foreach($records as $record){
        $data_arr[] = array(
            "ODP_ID" => $record->ODP_ID,
            "ODP_LOCATION" => $record->ODP_LOCATION,
            "ODP_STATUS" => $record->ODP_STATUS,
            "CREATEDATE" => $record->CREATEDATE
        );
    }

    $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
    ); 

    echo json_encode($response);
    exit;
  }
}
