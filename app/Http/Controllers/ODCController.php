<?php
namespace App\Http\Controllers;

use App\DA\MasterODC;
use Illuminate\Http\Request;

class ODCController extends Controller
{
  //odc
  public function index()
  {
      $data = MasterODC::getAll();
      // dd($data);
      return view('odc.index', compact('data'));
  }
  public function form($id)
  {
      $sto = MasterODC::getAllSTO();
      $data = MasterODC::getById($id);
      return view('odc.input', compact('data','sto'));
  }
  public function save($id, Request $req)
  {
      MasterODC::insertOrUpdate($id, $req);
      return redirect('/odc');
  }

}
