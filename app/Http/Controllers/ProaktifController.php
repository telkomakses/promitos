<?php
namespace App\Http\Controllers;

use DB;
use App\DA\ProaktifModel;
use App\DA\MitraModel;
use App\DA\MitosModel;
use App\DA\TematikModel;
use App\DA\MasterODC;
use App\DA\ApiModel;
use Illuminate\Http\Request;

class ProaktifController extends Controller
{

  public function index()
  {
    $data = ProaktifModel::getAll();
    return view('proaktif.index', compact('data'));
  }
  public function form($id)
  {
    $data = ProaktifModel::getByID($id);
    $mitra = MitraModel::getAktifAll();
    $tematik = TematikModel::getAktifAll();
    $sto = MasterODC::getAllSTO();
    $boq = self::get_proaktif_boq($data->proaktif_id);
    // dd($boq);
    return view('proaktif.form', compact('data','mitra','sto','tematik','boq'));
  }
  
  public static function get_proaktif_boq($id)
  {
    $akun = json_decode(ApiModel::post('https://marina.bjm.tomman.app/TOMMANAKUN', 'user=885870'));
    // dd($akun);
    // $akun = DB::table('akun')->where('user', '885870')->first();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/proactive/index.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'proaktif.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=".$akun->data->user."&password=".$akun->data->pwd."&btn_login=Log+In");
    $rough_content = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/proactive/show_initiation.php?project_id='.$id);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $table = $dom->getElementsByTagName('table')->item(4);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
              0 =>'designator',
              'uraian',
              'satuan',
              'telkom_material','telkom_jasa','mitra_material','mitra_jasa','volume','telkom_total','mitra_total');
    for ($i = 2, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
          $td = $cells->item($j);
            if($j==0 ){
              $optionTags = $td->getElementsByTagName('option');
              foreach ($optionTags as $tag){
                  if ($tag->hasAttribute("selected")){
                    $isi = $tag->nodeValue;
                  }
              }
            }else{
              $isi = $td->getElementsByTagName('input')->item(0)->getAttribute('value');
            }

            $data[$columns[$j]] = trim(@$isi);
        }
        // dd($data);
        $rs[] = $data;
    }
    return $rs;
  }
}
