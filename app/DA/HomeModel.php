<?php

namespace App\DA;

use App\DA\MitosModel;
use Illuminate\Support\Facades\DB;

class HomeModel
{
  public static function getData()
  {
    $tematik = DB::table('mitos_tematik')->where('status', 'Aktif')->get();
    $barchart = [['1.Budget'],['2.Real'],['3.Gap']];
    foreach($tematik as $no => $t){
      $tmt[$t->id] = ++$no;
      $tmtcat[] = $t->nama_tematik;
      $barchart[0][] = $t->budget;
      $barchart[1][] = 0;
      $barchart[2][] = -$t->budget;
    }
    $data = MitosModel::getQuery()->where('step_id', '!=', 99)->where('tematik_id', '!=', '-1')->get();
    foreach($data as $d){
      // $barchart[0][$tmt[$d->tematik_id]] += $d->nilai;
      // if(in_array($d->step_id,[4,5,6,7])){
          $barchart[1][$tmt[$d->tematik_id]]+= $d->nilai;
          $barchart[2][$tmt[$d->tematik_id]]+= $d->nilai;
      // }
    }
    // dd($tmtcat);
    return ['barchart'=>$barchart,'tmtcat'=>$tmtcat];
  }

}
