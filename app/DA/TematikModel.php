<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class TematikModel
{
  //mitra
  public static function getAll()
  {
    return DB::Table('mitos_tematik')->select('*', 'nama_tematik as text')->get();
  }
  public static function getAktifAll()
  {
    return DB::Table('mitos_tematik')->select('*', 'nama_tematik as text')->where('status', 'Aktif')->get();
  }
  public static function getById($id)
  {
    return DB::Table('mitos_tematik')->where('id', $id)->first();
  }
  public static function insertOrUpdate($id, $req)
  {
    if(self::getById($id)){
      DB::table('mitos_tematik')->where('id', $id)->update(["nama_tematik"=> $req->nama_tematik, "status" =>$req->status, "budget" =>$req->budget]);
    }else{
      $id = DB::table('mitos_tematik')->insertGetId(["nama_tematik" => $req->nama_tematik, "status" => $req->status, "budget" =>$req->budget]);
    }
    return $id;
  }
}
