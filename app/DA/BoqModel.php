<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class BoqModel
{
  public static function insertboq($id, $input)
  {
      DB::table('mitos_boq')->where('mitos_lop_id', $id)->delete();
      DB::table('mitos_boq')->insert($input);
  }
  
}
