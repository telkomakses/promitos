<?php

namespace App\DA;

use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;

class UploadModel implements ToModel
{
  public function model(array $row)
  {
    return [
      'name' => $row['0'],
    ];
  }
}
