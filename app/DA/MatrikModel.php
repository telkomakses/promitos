<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class MatrikModel
{
  public static function getMatrik1()
  {
    return DB::select("SELECT mitra_id ,mm.mitra,sum(l30) as l30,sum(l60) as l60,sum(l90) as l90,sum(golive) as golive
    FROM mitos_master_lop ml 
    left join mitos_mitra mm on ml.mitra_id=mm.id
    left join (select mitos_lop_id, 
    SUM(IF(TIMESTAMPDIFF(DAY,tgl_order,now())>=0 and TIMESTAMPDIFF(DAY,tgl_order,now())<=30 and status = 'Booked',1,0)) as l30, 
    SUM(IF(TIMESTAMPDIFF(DAY,tgl_order,now())>30 and TIMESTAMPDIFF(DAY,tgl_order,now())<=60 and status = 'Booked',1,0)) as l60, 
    SUM(IF(TIMESTAMPDIFF(DAY,tgl_order,now())>60 and status = 'Booked',1,0)) as l90, SUM(IF(status = 'Golive',1,0)) as golive
    from mitos_master_odp group by mitos_lop_id) oc on ml.id=oc.mitos_lop_id
    where ml.isHapus=0 GROUP BY ml.mitra_id ORDER BY ml.mitra_id");
  }
  public static function getMatrik1_detil($mitra,$l)
  {
    $sql = '';
    if($l=='l30'){
      $sql = 'TIMESTAMPDIFF(DAY,tgl_order,now())>=0 and TIMESTAMPDIFF(DAY,tgl_order,now())<=30';
    }else if($l=='l60'){
      $sql = 'TIMESTAMPDIFF(DAY,tgl_order,now())>30 and TIMESTAMPDIFF(DAY,tgl_order,now())<=60';
    }else{
      $sql = 'TIMESTAMPDIFF(DAY,tgl_order,now())>60';
    }
    return DB::table('mitos_master_lop')->select('*',DB::raw('(CASE WHEN tgl_golive = "0000-00-00 00:00:00" THEN TIMESTAMPDIFF(DAY,tgl_order,now()) ELSE TIMESTAMPDIFF(DAY,tgl_order,tgl_golive) END) as duration'))->leftJoin('mitos_master_odp','mitos_master_lop.id','=','mitos_master_odp.mitos_lop_id')
      ->where('mitra_id', $mitra)
      ->whereRaw($sql)
      ->where('mitos_master_odp.status', 'Booked')
      ->where('mitos_master_lop.isHapus', 0)
      ->get();
  }
  
}
