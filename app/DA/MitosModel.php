<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class MitosModel
{
    public static function getRecords($stepid,$start,$rowperpage,$columnName,$columnSortOrder,$searchValue){
        // dd($rowperpage);
        return self::getQuery()
        ->where('mitos_master_lop.step_id', $stepid)
        ->where('isHapus', 0)
        ->where('nama_lop', 'like', '%' .$searchValue . '%')
        ->orderBy($columnName,$columnSortOrder)
        ->skip($start)
        ->take($rowperpage)
        ->get();
    }
    public static function countTotalRecord($stepid){
        return DB::table('mitos_master_lop')->select('count(*) as allcount')
        ->where('mitos_master_lop.step_id', $stepid)->where('isHapus', 0)
        ->count();
    }
    public static function countTotalRecordswithFilter($stepid,$search){
        return DB::table('mitos_master_lop')->select('count(*) as allcount')
        ->where('mitos_master_lop.step_id', $stepid)->where('isHapus', 0)->where('nama_lop', 'like', '%' .$search . '%')
        ->count();
    }
    public static function getQuery(){
        return DB::table('mitos_master_lop')->select('mitos_master_lop.*', 'mitos_step.step', 'mitos_tematik.nama_tematik', 'mitos_mitra.mitra',DB::raw('(select sum((mb.hargajasa*mb.qty)+(mb.hargamaterial*mb.qty)) from mitos_boq mb where mitos_lop_id = mitos_master_lop.id) as nilai, coalesce(oc.Count, 0) as totalterminal'))
        ->leftJoin(DB::Raw('(
            select mitos_lop_id, count(*) as Count 
            from mitos_master_odp 
            group by mitos_lop_id
        ) oc'), 'mitos_master_lop.id','=','oc.mitos_lop_id')
        ->leftJoin('mitos_step','mitos_master_lop.step_id', '=', 'mitos_step.id')
        ->leftJoin('mitos_tematik','mitos_master_lop.tematik_id', '=', 'mitos_tematik.id')
        ->leftJoin('mitos_mitra','mitos_master_lop.mitra_id', '=', 'mitos_mitra.id');

    }
    public static function getByStepId($id)
    {
        return self::getQuery()->where('mitos_master_lop.step_id', $id)->where('isHapus', 0)->get();
    }
    public static function getAll()
    {
        return self::getQuery()->where('isHapus', 0)->get();
    }
    public static function getById($id)
    {
        return self::getQuery()->where('mitos_master_lop.id', $id)->first();
    }
    public static function insertMaster($param){
        return DB::table('mitos_master_lop')->insertGetId($param);
    }
    public static function updateMaster($id, $param){
        DB::table('mitos_master_lop')->where('id', $id)->update($param);
    }

}
