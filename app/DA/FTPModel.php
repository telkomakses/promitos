<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class FTPModel
{
    public static function getRecords($start,$rowperpage,$columnName,$columnSortOrder,$searchValue){
        return DB::table('master_odp')->orderBy($columnName,$columnSortOrder)
        ->where('ODP_LOCATION', 'like', '%' .$searchValue . '%')
        ->select('master_odp.*')
        ->skip($start)
        ->take($rowperpage)
        ->get();
    }
    public static function countTotalRecord(){
        return DB::table('master_odp')->select('count(*) as allcount')->count();
    }
    public static function countTotalRecordswithFilter($search){
        return DB::table('master_odp')->select('count(*) as allcount')->where('ODP_LOCATION', 'like', '%' .$search . '%')->count();
    }

}
