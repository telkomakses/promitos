<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class LogModel
{
    private static function getQuery(){
        return DB::table('mitos_log')->leftJoin('mitos_step','mitos_log.step_id', '=', 'mitos_step.id')->leftJoin('mitos_master_lop', 'mitos_log.mitos_lop_id','=', 'mitos_master_lop.id');
    }
    public static function getAllByMitosId($id)
    {
        return self::getQuery()->where('mitos_log.mitos_lop_id', $id)->get();
    }
    public static function getByMitosId($id)
    {
        return self::getQuery()->where('mitos_log.mitos_lop_id', $id)->orderBy('mitos_log.id', 'desc')->first();
    }
    public static function getById($id)
    {
        return self::getQuery()->where('mitos_log.mitos_lop_id', $id)->first();
    }
    public static function insertLog($insert){
        $last = self::getByMitosId($insert['mitos_lop_id']);
        if($last){
            $insert['start_date'] = $last->end_date;
        }else{
            $insert['start_date'] = DB::raw('now()');
        }
        DB::table('mitos_log')->insert($insert);
    }
}
