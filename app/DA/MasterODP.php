<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class MasterODP
{
  private static function getQuery(){
    return DB::table('mitos_master_odp')->select('mitos_master_odp.*',DB::raw('(CASE WHEN tgl_golive = "0000-00-00 00:00:00" THEN TIMESTAMPDIFF(DAY,tgl_order,now()) ELSE TIMESTAMPDIFF(DAY,tgl_order,tgl_golive) END) as duration'));
  }

  public static function update($id, $req)
  {
    DB::table('mitos_master_odp')->where('id', $id)->update(['status'=> $req->status]);
  }
  public static function getByIdLop($id)
  {
    return self::getQuery()->where('mitos_master_odp.mitos_lop_id', $id)->get();
  }
  public static function getById($id)
  {
    return self::getQuery()->where('mitos_master_odp.id', $id)->first();
  }
  public static function getByODC($sto,$odc)
  {
    return DB::table('mitos_master_odp')
      ->where('mitos_master_odp.sto',$sto)
      ->where('mitos_master_odp.odc',$odc)->orderBy('index_terminal', 'desc')->get();
  }
  public static function getAllByOdc($jenis_terminal,$sto,$odc)
  {
    return DB::table('mitos_master_odp')->leftJoin('mitos_master_lop','mitos_master_odp.mitos_lop_id', '=', 'mitos_master_lop.id')->select('mitos_master_lop.id_regu', 'mitos_master_odp.*')->where('jenis_terminal',$jenis_terminal)
      ->where('mitos_master_odp.sto',$sto)
      ->where('mitos_master_odp.odc',$odc)->orderBy('index_terminal', 'desc')->get();
  }
  public static function getCountFree($req)
  {
    return DB::table('mitos_master_odp')->where('jenis_terminal',$req->jenis_terminal)
      ->where('mitos_master_odp.sto',$req->sto)
      ->where('mitos_master_odp.odc',$req->odc)
      ->where('mitos_master_odp.status','Free')->count();
  }
  public static function getFreeRow($req)
  {
    return DB::table('mitos_master_odp')->where('jenis_terminal',$req->jenis_terminal)
      ->where('mitos_master_odp.sto',$req->sto)
      ->where('mitos_master_odp.odc',$req->odc)
      ->where('mitos_master_odp.status','Free')->get();
  }
  public static function getFreeRowSingle($jt,$sto,$odc,$id_regu)
  {
    return DB::table('mitos_master_odp')->leftJoin('mitos_master_lop', 'mitos_master_odp.mitos_lop_id', '=','mitos_master_lop.id')->select('mitos_master_odp.*')->where('mitos_master_odp.jenis_terminal',$jt)
      ->where('mitos_master_odp.sto',$sto)
      ->where('mitos_master_odp.odc',$odc)
      ->where('mitos_master_lop.id_regu',$id_regu)
      ->where('mitos_master_odp.status','Free')->orderBy('mitos_master_odp.id', 'asc')->first();
  }
  public static function getLastIndex($jt,$sto,$odc)
  {
    return DB::table('mitos_master_odp')->select('mitos_master_odp.index_terminal')->where('jenis_terminal',$jt)
      ->where('mitos_master_odp.sto',$sto)
      ->where('mitos_master_odp.odc',$odc)->orderBy('id', 'desc')->first();
  }
  public static function updateFreeSingle($update){
    DB::table('mitos_master_odp')->where('id',$update['id'])->update($update['update']);
  }
  // public static function updateFree($req, $update, $limit)
  // {
  //   DB::table('mitos_master_odp')->where('jenis_terminal',$req->jenis_terminal)
  //     ->where('sto',$req->sto)
  //     ->where('odc',$req->odc)
  //     ->where('status','Free')->orderBy('id', 'asc')->limit($limit)->update($update);
  // }
  public static function insertNewSlot($insert)
  {
    DB::table('mitos_master_odp')->insert($insert);
  }
  public static function insertNewSlotGetId($insert)
  {
    return DB::table('mitos_master_odp')->insertGetId($insert);
  }

  public static function setFreeByLopId($id)
  {
    DB::table('mitos_master_odp')->where('mitos_lop_id',$id)->update(['status'=>'Free']);
  }
}
