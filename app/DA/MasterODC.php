<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class MasterODC
{
  private static function getQuery(){
    return DB::table('mitos_odc_master')->select('mitos_odc_master.*');
  }
  public static function getById($id)
  {
    return self::getQuery()->where('mitos_odc_master.id', $id)->first();
  }
  public static function getAll()
  {
    return self::getQuery()->orderBy('id', 'desc')->get();
  }
  public static function getAllODC($sto)
  {
    return DB::table('mitos_odc_master')->select('odc as id', 'odc as text')->where('sto',$sto)->orderBy('odc', 'asc')->get();
  }
  public static function getAllSTO()
  {
    return DB::select('SELECT COUNT(*) AS `Baris`, sto as text, sto as id FROM `mitos_odc_master` GROUP BY `sto` ORDER BY `sto`');
  }
  public static function insertOrUpdate($id, $req)
  {
    $input = $req->only('datel','sto','odc','segment','tipe','kap','latitude','longitude','sektor','tahun','tipe_kabel');
    // dd($input);
    if(self::getById($id)){
      DB::table('mitos_odc_master')->where('id', $id)->update($input);
    }else{
      $id = DB::table('mitos_odc_master')->insertGetId($input);
    }
    return $id;
  }
  
}
