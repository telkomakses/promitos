<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class MitraModel
{
  //mitra
  public static function getAll()
  {
    return DB::Table('mitos_mitra')->select('*', 'mitra as text')->get();
  }
  public static function getAktifAll()
  {
    return DB::Table('mitos_mitra')->select('*', 'mitra as text')->where('status', 'Aktif')->get();
  }
  public static function getById($id)
  {
    return DB::Table('mitos_mitra')->where('id', $id)->first();
  }
  public static function insertOrUpdate($id, $req)
  {
    if(self::getById($id)){
      DB::table('mitos_mitra')->where('id', $id)->update(["mitra"=> $req->mitra, "status" =>$req->status]);
    }else{
      $id = DB::table('mitos_mitra')->insertGetId(["mitra" => $req->mitra, "status" => $req->status]);
    }
    return $id;
  }

  //tim mitra
  // public static function savetim_Mitra($req)
  // {
  //   DB::Table('regu')->insert([
  //     'uraian'     => $req->uraian,
  //     'witel'      => 'kalsel',
  //     'mitra'      => $req->mitra,
  //     'nik1'       => $req->nik1,
  //     'nik2'       => $req->nik2,
  //     'jenis_regu' => 'mpromise'
  //   ]);
  // }
  // //tes comment2
  // public static function updatetim_mitra($req, $id)
  // {
  //   DB::Table('regu')->where('id_regu', $id)->update([
  //     'uraian'     => $req->uraian,
  //     'mitra'      => $req->mitra,
  //     'nik1'       => $req->nik1,
  //     'nik2'       => $req->nik2,
  //     'jenis_regu' => 'mpromise'
  //   ]);
  // }

  // public static function list_tim()
  // {
  //   return DB::Table('regu')->where('jenis_regu', '=', 'mpromise')->get();
  // }

  // public static function listMitra()
  // {
  //   return DB::Table('mpromise_mitra')->select('*', 'mitra as text')->get();
  // }
}
