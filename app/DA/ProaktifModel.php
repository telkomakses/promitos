<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class ProaktifModel
{
  public static function getAll()
  {
    return DB::table('proaktif_project')->select('proaktif_project.*', 'mitos_master_lop.id as flag')->leftJoin('mitos_master_lop', 'proaktif_project.proaktif_id','=', DB::raw('mitos_master_lop.proaktif_id AND mitos_master_lop.isHapus = 0'))->where('portofolio', 'Konstruksi')->where('nama_project','not like', '%PT2%')->whereIn('status_project', ['Active', 'Init'])
    ->get();
  }
  public static function getByID($id)
  {
    return DB::table('proaktif_project')->where('id', $id)
    ->first();
  }
  
}
