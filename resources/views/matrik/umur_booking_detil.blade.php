@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    body .modal-xl {
        width: 1250px;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i></span>Matrik Umur Booking ODP
</h1>
@endsection
@section('title', 'ODP')
@section('content')
<div class="panel m-t-2 col-sm-12">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Label</th>
                <th>Lop</th>
                
                <th>Status</th>
                <th>Koordinat</th>
                <th>Tgl Order</th>
                <th>Durasi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ "$d->jenis_terminal-$d->sto-$d->odc/$d->index_terminal" }}</td>
                    <td>{{ $d->nama_lop }}</td>
                    
                    <td>{{ $d->status }}</td>
                    <td>{{ $d->koordinat_kml }}</td>
                    <td>{{ $d->tgl_order }}</td>
                    <td>{{ $d->duration }} Hari</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
</script>
@endsection
