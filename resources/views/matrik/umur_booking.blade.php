@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    body .modal-xl {
        width: 1250px;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i></span>Matrik Umur Booking ODP
</h1>
@endsection
@section('title', 'ODP')
@section('content')
<div class="panel m-t-2 col-sm-12">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th rowspan="2" class="valign-middle text-center">#</th>
                <th rowspan="2" class="valign-middle text-center">Mitra</th>
                <th rowspan="2" class="valign-middle text-center">Total</th>
                <th colspan="3" class="text-center">Booking</th>
                <th rowspan="2" class="valign-middle text-center">Golive</th>
            </tr>
            <tr>
                <th class="text-center">1-30D</th>
                <th class="text-center">31-60D</th>
                <th class="text-center">>60D</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->mitra ?:"#N/A" }}</td>
                    <td class="text-right">{{ number_format($d->l30+$d->l60+$d->l90+$d->golive) }}</td>
                    <td class="text-right">
                      @if($d->l30)
                      <a href="/umurodp/{{ $d->mitra_id }}/l30" class="label label-success">{{ number_format($d->l30) }}</a>
                      @else
                      -
                      @endif
                    </td>
                    <td class="text-right">
                      @if($d->l60)
                      <a href="/umurodp/{{ $d->mitra_id }}/l60" class="label label-success">{{ number_format($d->l60) }}</a>
                      @else
                      -
                      @endif
                    </td>
                    <td class="text-right">
                      @if($d->l90)
                      <a href="/umurodp/{{ $d->mitra_id }}/l90" class="label label-success">{{ number_format($d->l90) }}</a>
                      @else
                      -
                      @endif
                    </td>
                    <td class="text-right">{{ number_format($d->golive) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
</script>
@endsection
