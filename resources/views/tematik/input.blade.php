@extends('layout')
@section('heading')
<h1><a href="/tematik" class="ion-arrow-left-a"></a> Form Tematik {{ $data->nama_tematik or '' }}</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" enctype="multipart/form-data" id="form-mitra">
    <div class="form-group form-message-dark">
        <label for="nama_tematik" class="col-md-2 control-label">Nama Tematik</label>
        <div class="col-md-9">
            <input type="text" class="form-control nama_tematik" name="nama_tematik" id="nama_tematik" value="{{ $data->nama_tematik or '' }}" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="budget" class="col-md-2 control-label">Budget</label>
        <div class="col-md-9">
            <input type="text" class="form-control budget" name="budget" id="budget" value="{{ $data->budget or '' }}" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="status" class="col-md-2 control-label">Status</label>
        <div class="col-md-9">
            <input type="text" class="form-control status" name="status" id="status" value="{{ $data->status or '' }}" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn">Simpan</button>
        </div>
    </div>
</form>
@endsection
@section('js')
<script>
    $(function() {
        $('#form-mitra').pxValidate();
        $('#status').select2({
            data:[{"id":"Aktif","text":"Aktif"},{"id":"Tidak Aktif","text":"Tidak Aktif"}],
            placeholder:"Pilih Status"
        });
    });
</script>
@endsection