@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Mitra / </span>List </h1>
    <a href="/mitra/input" class=" pull-xs-right"><button type="button" class="btn btn-primary btn-rounded">Input</button></a>
@endsection
@section('title', 'List Mitra')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Mitra</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->mitra }}</td>
                    <td>{{ $d->status }}</td>
                    <td>
                        <a href="/mitra/{{ $d->id }}" class="btn btn-xs btn-info detail"><i class="ion-update"></i>&nbsp;&nbsp;Update</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endsection

@section('js')
@endsection
