@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Theme</i>

</h1>
<button type="button" class="btn btn-info pull-right btn-save"><i class="ion-soup-can"></i> Simpan</button>
@endsection
@section('content')
	<?php
		$theme = array('asphalt','purple-hills','adminflare','dust','frost','fresh','silver','clean','white','candy-black','candy-blue','candy-red','candy-orange','candy-green','candy-purple','candy-cyan','mint-dark','dark-blue','dark-red','dark-orange','dark-green','dark-purple','dark-cyan','darklight-blue','darklight-red','darklight-orange','darklight-green','darklight-purple','darklight-cyan');
	?>
	<form class="form-horizontal" method="post" id="form-theme" action="/theme">
	  <div class="px-demo-themes-list clearfix bg-primary">
	  	@foreach($theme as $t)
	  	<?php
	  		$img = '/themes/'.$t.'.png';
	  	?>
			<label class="px-demo-themes-item">
				<input type="radio" class="px-demo-themes-toggler" name="theme" value="{{ $t }}">
				<img src="{{ $img }}" class="px-demo-themes-thumbnail">
				<div class="px-demo-themes-title font-weight-semibold">
					<span class="text-white">{{$t}}</span>
					<div class="bg-primary">
					</div>
				</div>
			</label>
			@endforeach
		</div>
	</form>
@endsection

@section('js')
<script type="text/javascript">
	
	$('.btn-save').click(function(){
		$('#form-theme').submit();
	});
</script>
@endsection
