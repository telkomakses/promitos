@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>ODC / </span>List </h1>
    <a href="/odc/input" class=" pull-xs-right"><button type="button" class="btn btn-primary btn-rounded">Input</button></a>
@endsection
@section('title', 'List ODC')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Datel</th>
                <th>STO</th>
                <th>ODC</th>
                <th>Segment</th>
                <th>Tipe</th>
                <th>Kap</th>
                <th>Koordinat</th>
                <th>Sektor</th>
                <th>Tahun</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->datel }}</td>
                    <td>{{ $d->sto }}</td>
                    <td>{{ $d->odc }}</td>
                    <td>{{ $d->segment }}</td>
                    <td>{{ $d->tipe }}</td>
                    <td>{{ $d->kap }}</td>
                    <td>{{ $d->latitude }}, {{ $d->longitude }}</td>
                    <td>{{ $d->sektor }}</td>
                    <td>{{ $d->tahun }}</td>
                    <td>
                        <a href="/odc/{{ $d->id }}" class="btn btn-xs btn-info detail"><i class="ion-update"></i>&nbsp;&nbsp;Update</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('#datatables').dataTable();
        $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
    });
</script>
@endsection
