@extends('layout')
@section('heading')
<h1><a href="/odc" class="ion-arrow-left-a"></a> Form odc {{ $data->odc or '' }}</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" enctype="multipart/form-data" id="form-mitra">
    <div class="form-group form-message-dark">
        <label for="datel" class="col-md-2 control-label">Datel</label>
        <div class="col-md-9">
            <input type="text" class="form-control datel" name="datel" id="datel" value="{{ $data->datel or '' }}" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="sto" class="col-md-2 control-label">STO</label>
        <div class="col-md-9">
            <input type="text" class="form-control sto" name="sto" id="sto" value="{{ $data->sto or '' }}" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="odc" class="col-md-2 control-label">ODC</label>
        <div class="col-md-9">
            <input type="text" class="form-control odc" name="odc" id="odc" value="{{ $data->odc or '' }}" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="segment" class="col-md-2 control-label">Segment</label>
        <div class="col-md-9">
            <input type="text" class="form-control segment" name="segment" id="segment" value="{{ $data->segment or '' }}" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="tipe" class="col-md-2 control-label">Tipe</label>
        <div class="col-md-9">
            <input type="text" class="form-control tipe" name="tipe" id="tipe" value="{{ $data->tipe or '' }}" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="kap" class="col-md-2 control-label">KAP</label>
        <div class="col-md-9">
            <input type="text" class="form-control kap" name="kap" id="kap" value="{{ $data->kap or '' }}" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="latitude" class="col-md-2 control-label">Latitude</label>
        <div class="col-md-9">
            <input type="text" class="form-control latitude" name="latitude" id="latitude" value="{{ $data->latitude or '' }}" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="longitude" class="col-md-2 control-label">Longitude</label>
        <div class="col-md-9">
            <input type="text" class="form-control longitude" name="longitude" id="longitude" value="{{ $data->longitude or '' }}" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="sektor" class="col-md-2 control-label">Sektor</label>
        <div class="col-md-9">
            <input type="text" class="form-control sektor" name="sektor" id="sektor" value="{{ $data->sektor or '' }}" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="tahun" class="col-md-2 control-label">Tahun</label>
        <div class="col-md-9">
            <input type="text" class="form-control tahun" name="tahun" id="tahun" value="{{ $data->tahun or '' }}" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="tipe_kabel" class="col-md-2 control-label">Tipe Kabel</label>
        <div class="col-md-9">
            <input type="text" class="form-control tipe_kabel" name="tipe_kabel" id="tipe_kabel" value="{{ $data->tipe_kabel or '' }}" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn">Simpan</button>
        </div>
    </div>
</form>
@endsection
@section('js')
<script>
    $(function() {
        $('#form-mitra').pxValidate();
        $('#sto').select2({
            data:<?= json_encode($sto); ?>,
            placeholder:"Pilih STO"
        });
    });
</script>
@endsection