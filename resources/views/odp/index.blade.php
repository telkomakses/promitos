@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<div class="row">
    <div class="col-md-8">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>ODP / </span>List 
        </h1>
    </div>
    <div class="col-md-2">
        <input type="text" name="sto" value="{{ Request::segment(2) }}" class="pull-right m-r-1" id="sto"/>
    </div>
    <div class="col-md-2">
        <input type="text" name="odc" value="{{ Request::segment(3) }}" class="pull-right m-r-1" id="odc"/>
    </div>
    <!-- <div class="col-md-2 dropdown">
      <a href="/odp/input" class=" pull-xs-right"><button type="button" class="btn btn-primary btn-rounded">Register</button></a>
    </div> -->
</div>
@endsection
@section('title', 'List ODC')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Terminal</th>
                <th>koordinat_kml</th>
                <th>Tgl Order</th>
                <th>Status</th>
                <th>Tgl Golive</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->jenis_terminal }}-{{ $d->sto }}-{{ $d->odc }}/{{ $d->index_terminal }}</td>
                    <td>{{ $d->koordinat_kml }}</td>
                    <td>{{ $d->tgl_order }}</td>
                    <td>{{ $d->status }}</td>
                    <td>{{ $d->tgl_golive }}</td>
                    <td>
                        <a href="/odp/{{ $d->id }}" class="btn btn-xs btn-info detail"><i class="ion-update"></i>&nbsp;&nbsp;Update</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('#datatables').dataTable();
        $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
        var segments = $(location).attr('href').split( '/' );
        var p_odc = segments[5];
        var p_sto = segments[4];
        $('#sto').select2({
            data:$.merge([{'id':'0','text':'Pilih STO'}],<?= json_encode($s_sto); ?>),
            placeholder:'Select STO',

        }).change(function(){
            window.location.href = document.location.origin+"/odp/"+this.value+"/"+p_odc;
        });
        $('#odc').select2({
            data:$.merge([{'id':'0','text':'Pilih ODC'}],<?= json_encode($s_odc); ?>),
            placeholder:'Select ODC',

        }).change(function(){
            window.location.href = document.location.origin+"/odp/"+p_sto+"/"+this.value;
        });
    });
</script>
@endsection
