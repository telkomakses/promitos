@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<div class="row">
    <div class="col-md-8">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>ODP / </span>FTP 
        </h1>
    </div>
</div>
@endsection
@section('title', 'List ODP FTP')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
    </table>
</div>
</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    function render(data, type, row, meta){

      if(type === 'display'){
          data = '<a href="/en/state/?id=' + row.ODP_ID + '" class="label label-primary">' + data + '</a>';
      }

      return data;
    }
    $(function() {
        $('#datatables').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/master_odp/get",
            columns: 
            [
               { "data": "ODP_ID", "name": "ODP ID", "title": "ODP ID", "render": render },
               { "data": "ODP_LOCATION", "name": "ODP LOCATION", "title": "ODP LOCATION" },
               { "data": "ODP_STATUS", "name": "ODP STATUS", "title": "ODP STATUS" },
               { "data": "CREATEDATE", "name": "CREATEDATE", "title": "CREATEDATE" },
            ],
            responsive: true,
            "order": [[ 0, "desc" ]]
          });
    });
</script>
@endsection
