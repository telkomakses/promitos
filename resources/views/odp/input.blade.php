@extends('layout')
@section('heading')
<h1><a href="/odc" class="ion-arrow-left-a"></a> Form {{ $data->jenis_terminal or '' }}-{{ $data->sto or '' }}-{{ $data->odc or '' }}/{{ $data->index_terminal or '' }}</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" enctype="multipart/form-data" id="form-mitra">
    <div class="form-group form-message-dark">
        <label for="jenis_terminal" class="col-md-2 control-label">Jenis Terminal</label>
        <div class="col-md-9">
            <input type="text" class="form-control jenis_terminal" name="jenis_terminal" id="jenis_terminal" value="{{ $data->jenis_terminal or '' }}" readonly>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="sto" class="col-md-2 control-label">STO</label>
        <div class="col-md-9">
            <input type="text" class="form-control sto" name="sto" id="sto" value="{{ $data->sto or '' }}" readonly>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="odc" class="col-md-2 control-label">ODC</label>
        <div class="col-md-9">
            <input type="text" class="form-control odc" name="odc" id="odc" value="{{ $data->odc or '' }}" readonly>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="index_terminal" class="col-md-2 control-label">Index Terminal</label>
        <div class="col-md-9">
            <input type="text" class="form-control index_terminal" name="index_terminal" id="index_terminal" value="{{ $data->index_terminal or '' }}" readonly>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="koordinat_kml" class="col-md-2 control-label">Koordinat</label>
        <div class="col-md-9">
            <input type="text" class="form-control koordinat_kml" name="koordinat_kml" id="koordinat_kml" value="{{ $data->koordinat_kml or '' }}" readonly>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="tgl_order" class="col-md-2 control-label">Tgl Order</label>
        <div class="col-md-9">
            <input type="text" class="form-control tgl_order" name="tgl_order" id="tgl_order" value="{{ $data->tgl_order or '' }}" readonly>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="status" class="col-md-2 control-label">Status</label>
        <div class="col-md-9">
            <input type="text" class="form-control status" name="status" id="status" value="{{ $data->status or '' }}" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn">Simpan</button>
        </div>
    </div>
</form>
@endsection
@section('js')
<script>
    $(function() {
        $('#form-mitra').pxValidate();
        $('#status').select2({
            data:[{'id':'Free','text':'Free'},{'id':'Booked','text':'Booked'},{'id':'Golive','text':'Golive'}],
            placeholder:"Pilih Status"
        });
    });
</script>
@endsection