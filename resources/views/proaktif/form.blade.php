@extends('layout')
@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>{{ $data ? 'Register LOP' : 'Edit LOP' }}</span>
</h1>
@endsection
@section('title', $data ? 'Register' : 'Edit')
@section('content')

<?php
    $nama = explode('-', $data->nama_project);
?>
<form method="post" id="formRegister" enctype="multipart/form-data">
    <input type="hidden" name="proaktif_id" value="{{ $data->proaktif_id }}">
    <input type="hidden" name="boq" value="{{ json_encode($boq) }}">
    <div class="row">
        <div class="col-sm-4 form-group form-message-dark">
          <label for="jenis_pekerjaan">Jenis Pekerjaan</label>
          <select name="jenis_pekerjaan" id="jenis_pekerjaan" class="form-control" required>
                <option value="PT3" {{ str_contains($data->nama_project, 'OSP')?'selected':'' }}>PT3</option>
                <option value="NODE-B" {{ str_contains($data->nama_project, 'NODE')?'selected':'' }}>NODE-B</option>
                <option value="FEEDERISASI" {{ str_contains($data->nama_project, 'FEE')?'selected':'' }}>FEEDERISASI</option>
            </select>
        </div>

        <div class="col-sm-2 form-group form-message-dark">
          <label for="sto">STO</label>
          <input type="text" name="sto" id="sto" class="form-control" value="{{ @$nama[1] }}" required>
        </div>
        <div class="col-sm-2 form-group form-message-dark">
          <label for="odc">ODC</label>
          <input type="text" name="odc" id="odc" class="form-control" value="{{ @$nama[2] }}" required>
        </div>
        <div class="col-sm-4 form-group form-message-dark">
          <label for="nama_lop">Nama Site</label>
          <input type="text" name="nama_lop" id="nama_lop" class="form-control" value="{{ @$data->nama_project }}" required>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group form-message-dark">
          <label for="mitra">Mitra</label>
          <input type="text" name="mitra_id" id="mitra" class="form-control" value="1" required>
        </div>
        <div class="col-sm-6 form-group form-message-dark">
          <label for="tematik">Jenis Project/Tematik</label>
          <input type="text" name="tematik_id" id="tematik" class="form-control" required>
        </div>
    </div>
    <div class="row m-t-2">
        <div class="form-group form-message-dark">
            <label for="BOQ_Unwizing" class="col-md-2 control-label">KML Booking</label>
            <div class="col-md-10">
                <label id="kml_plan" class="custom-file px-file" for="kml_plani">
                    <input type="file" id="kml_plani" class="custom-file-input" name="kml_plan" {{ @$data->kml_plan?'':'required' }}>
                    <span class="custom-file-control form-control">Choose file...</span>
                    <div class="px-file-buttons">
                        <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                        <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                    </div>
                </label>
            </div>
        </div>
    </div>
        <div class="form-group m-t-2">
            <div class="">
                <button type="submit" class="btn btn-primary pull-right"><i class="ion-soup-can"></i> Simpan</button>
            </div>
        </div>
</form>
<br>
<br>
<div class="table-responsive table-primary">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Designator</th>
                <th>Uraian</th>
                <th>Satuan</th>
                <th>Material</th>
                <th>Jasa</th>
                <th>Volume</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach($boq as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d['designator'] }}</td>
                    <td>{{ $d['uraian'] }}</td>
                    <td>{{ $d['satuan'] }}</td>
                    <td class="text-right">{{ $d['telkom_material'] }}</td>
                    <td class="text-right">{{ $d['telkom_jasa'] }}</td>
                    <td class="text-right">{{ $d['volume'] }}</td>
                    <td class="text-right">{{ $d['telkom_total'] }}</td>

                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('js')
<script>
    $(function() {
        $('#tab-resize-pills').pxTabResize();
        $('#BOQ_Unwizing').pxFile();
        $('#kml_plan').pxFile();
        $('#formRegister').pxValidate();
        $('#jenis_pekerjaan').select2();
        $('#tematik').select2({
            placeholder:'Select Tematik',
            data:<?= json_encode($tematik); ?>
        });
        $('#mitra').select2({
            placeholder:'Select Mitra',
            data:<?= json_encode($mitra); ?>
        });
        $('#sto').select2({
            placeholder:'Select STO',
            data:<?= json_encode($sto); ?>
        });
        var odc = $('#odc').select2({
            placeholder:'Pilih ODC',
            data:[{'id':'','text':'pilih'}]
        });
        $.getJSON('/getOdcBySTO/{{$nama[1]}}', function(data){
              odc.select2({
                data:data,
                placeholder:'Pilih ODC',
                allowClear:true
              });
            });
        $('#sto').change(function(e){
            $.getJSON('/getOdcBySTO/'+e.target.value, function(data){
              odc.select2({
                data:data,
                placeholder:'Pilih ODC',
                allowClear:true
              });
            });
        });
    });

</script>
@endsection
