@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Proaktif LOP / </span>List
</h1>
@endsection
@section('title', 'List Proaktif')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama Project</th>
                <th>Pid</th>
                <th>SAP ID</th>
                <th>Nomor Kontrak</th>
                <th>PM</th>
                <th>Tgl.Mulai</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->nama_project }}</td>
                    <td>{{ $d->pid }}</td>
                    <td>{{ $d->sapid }}</td>
                    <td>{{ $d->nomor_kontrak }}</td>
                    <td>{{ $d->pm }}</td>
                    <td>{{ $d->tgl_mulai }}</td>
                    <td>
                        @if($d->flag)
                            <span class="label label-success">Booked</span>
                        @else
                        <a href="/proaktif/{{ $d->id }}"><span class="btn btn-sm btn-info"><i class="ion ion-ios-cloud-download"></i>&nbsp;&nbsp;Book</span></a>
                        @endif

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('#datatables').dataTable();
        $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
    });
</script>
@endsection
