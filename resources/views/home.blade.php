@extends('layout')
@section('heading')
<div class="row">
    <div class="col-md-9">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Dashboard</span>
        </h1>
    </div>
    <div class="col-md-3">
        <input type="text" name="tematik" class=" pull-right" id="filter"/>
    </div>  
</div>
@endsection
@section('css')
<style type="text/css">
  tr > th {
    text-align: center;
  }
</style>
@endsection
@section('title', 'Dashboard')
@section('content')
<!--     
<div id="tematikbar" style="height: 250px" class="col-sm-6"></div>
<div id="mitrabar" style="height: 250px" class="col-sm-6"></div> -->
@endsection
@section('js')

@endsection