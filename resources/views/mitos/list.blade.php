@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    body .modal-xl {
        width: 1250px;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i></span>List
</h1>
@if(Request::segment(2)==1)
<a href="/registerLop" class="pull-right"><span class="btn btn-info"><i class="ion ion-plus"></i> Register Lop</span></a>
@endif
@endsection
@section('title', 'List')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        
    </table>
</div>
</div>
</div>

<div class="modal" id="modal-info">
  <div class="modal-dialog modal-xl" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title tittle">Modal title</h4>
      </div>
      <div class="modal-body" id="detilContent">
        <p>One fine body…</p>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    function render(data, type, row, meta){
      if(type === 'display'){
          data = '<a href="/progress/' + row.id + '" class="btn btn-xs btn-success"><i class="ion ion-compose"></i>&nbsp;&nbsp;Update</a>';
      }

      return data;
    }
    $(function() {
        $('#datatables').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/listdata/{{ Request::segment(2) }}",
            columns: 
            [
               { "data": "nama_lop", "name": "nama_lop", "title": "Lop"},
               { "data": "jenis_pekerjaan", "name": "jenis_pekerjaan", "title": "Jenis Pekerjaan" },
               { "data": "mitra", "name": "mitra", "title": "Mitra" },
               { "data": "totalterminal", "name": "totalterminal", "title": "Booking" },
               { "data": "id", "name": "id", "title": "ACT", "render": render }
            ],
            responsive: true,
            "order": [[ 0, "desc" ]]
          });
    });
</script>
@endsection
