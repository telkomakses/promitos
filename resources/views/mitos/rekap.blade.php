@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    body .modal-xl {
        width: 1250px;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i></span>Rekap
</h1>
@endsection
@section('title', 'List')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Tematik</th>
                <th>Mitra</th>
                <th>Nilai</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->nama_lop }}</td>
                    <td>{{ $d->nama_tematik }}</td>
                    <td>{{ $d->mitra }}</td>
                    <td>{{ number_format($d->nilai) }}</td>
                    <td>{{ $d->step }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        function render(data, type, row, meta){

          if(type === 'display'){
              data = '<span class="label label-primary">' + data + '</span>';
          }

          return data;
        }
        $(function() {
            $('#datatables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "/master_odp/get",
                columns: 
                [
                   { "data": "nama_lop", "name": "nama_lop", "title": "Nama Lop"},
                   { "data": "nama_tematik", "name": "nama_tematik", "title": "Tematik" },
                   { "data": "mitra", "name": "mitra", "title": "Mitra" },
                   { "data": "nilai", "name": "nilai", "title": "Nilai" },
                   { "data": "step", "name": "step", "title": "Status", "render": render},
                ],
                responsive: true,
                "order": [[ 0, "desc" ]]
              });
        });
    });
</script>
@endsection
