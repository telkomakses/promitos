@extends('layout')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Verifikasi SDI</span>
</h1>
@endsection
@section('title', 'Verifikasi SDI')
@section('content')
<form method="post" id="form-register" enctype="multipart/form-data" action="/save_verifikasi_sdi/{{ Request::segment(2) }}">
    <!-- <div class="form-group form-message-dark">
        <label for="status" class="col-md-2 control-label">Status</label>
        <div class="col-md-9">
            <input type="text" name="status" id="status" class="form-control" required/>
        </div>
    </div> -->

    <div class="row">
        <div class="col-sm-6 form-group form-message-dark">
          <label for="grid-input-16">Status</label>
          <input type="text" name="status" id="status" class="form-control" required/>
        </div>
    </div>
    <!-- 
    <div class="form-group form-message-dark">
        <label for="prioritas" class="col-md-2 control-label">Prioritas</label>
        <div class="col-md-9">
            <input type="text" name="prioritas" id="prioritas" class="form-control" required/>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="catatan" class="col-md-2 control-label">Catatan</label>
        <div class="col-md-9">
            <textarea name="catatan" id="catatan" class="form-control" required></textarea>
        </div>
    </div> -->
    
    <div class="form-group form-message-dark">
        <label for="catatan" class="col-md-2 control-label">Catatan</label>
        <div class="col-md-9">
            <textarea name="catatan" id="catatan" class="form-control" required></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="">
            <button type="submit" class="btn btn-primary pull-right"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
<div class="panel m-t-2 col-sm-12">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Label</th>
                <th>Terminal</th>
                <th>STO</th>
                <th>ODC</th>
                <th>Index</th>
                <th>Status</th>
                <th>Koordinat</th>
                <th>Tgl Order</th>
                <th>Durasi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($booked as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ "$d->jenis_terminal-$d->sto-$d->odc/$d->index_terminal" }}</td>
                    <td>{{ $d->jenis_terminal }}</td>
                    <td>{{ $d->sto }}</td>
                    <td>{{ $d->odc }}</td>
                    <td>{{ $d->index_terminal }}</td>
                    <td>{{ $d->status }}</td>
                    <td>{{ $d->koordinat_kml }}</td>
                    <td>{{ $d->tgl_order }}</td>
                    <td>{{ $d->duration }} Hari</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        // $('#upload_lop').pxFile();
        $('#status').select2({
            placeholder:'Select Status',
            data:[{'id':'6','text':'Approve'},{'id':'4','text':'Reject'},{'id':'99','text':'Pending'}]
        });
    });
</script>
@endsection
