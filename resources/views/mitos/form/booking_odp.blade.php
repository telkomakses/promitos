@extends('layout')
@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Booking ODP,CLOSURE,CGL</span>
</h1>
@endsection
@section('title', 'Booking')
@section('content')
<ul class="nav nav-pills" id="tab-resize-pills">
  <li><a href="#tab-resize-pills-1" data-toggle="tab">From KML</a></li>
  <li><a href="#tab-resize-pills-2" data-toggle="tab">Booking Manual</a></li>
</ul>
<div class="tab-content">
  <div class="tab-pane" id="tab-resize-pills-1">
    @if($odp)
    @if(is_array($odp['Folder']))
    <form method="post" id="formBookingOdp" action="/save_request_odp/{{ Request::segment(2) }}">
      <div class="row">
        <div class="col-sm-3 form-group form-message-dark">
          <label for="grid-input-16">Jenis Terminal</label>
          <input type="text" name="jenis_terminal" class="form-control jenis_terminal" required>
        </div>
        <div class="col-sm-1 form-group form-message-dark">
          <label for="grid-input-17">STO</label>
          <input type="text" name="sto" id="grid-input-17" class="form-control" value="{{ $data->sto or '' }}" required>
        </div>
        <div class="col-sm-1 form-group form-message-dark">
          <label for="grid-input-18">ODC</label>
          <input type="text" name="odc" id="grid-input-18" class="form-control" value="{{ $data->odc or '' }}" required>
        </div>
        <div class="col-sm-2 form-group form-message-dark">
          <label for="jumlah_terminal">Jumlah Terminal</label>
          <input type="text" name="jumlah_terminal" id="jumlah_terminal" class="form-control" required>
        </div>
        <div class="col-sm-3 form-group form-message-dark">
          <label for="folder_kml">Folder KML</label>
          <select name="folder_kml" id="folder_kml" class="form-control" required>
              <option></option>
              @foreach($odp['Folder'] as $i => $f)
              <option value="{{ $i }}">{{ @$f['name'] }}</option>
              @endforeach
          </select>
        </div>
        <input type="hidden" name="terminal" id="alpro">
        <div class="col-sm-2 form-group" style="margin-top:25px;">
          <button type="submit" class="btn btn-primary">Book</button>
          @if(count($booked))
          <button type="button" id="nextstep" class="btn btn-info pull-right"><i class="ion-soup-can"></i> Next Step</button>
          @endif
        </div>
        
      </div>
    </form>
    @else
      KML tidak Standart.
    @endif
    @else
      KML Tidak ada.
    @endif
  </div>
  <div class="tab-pane" id="tab-resize-pills-2">
    <form method="post" id="formBookingOdpSingle" action="/save_request_odp_single/{{ Request::segment(2) }}">
      <div class="row">
        <div class="col-sm-3 form-group form-message-dark">
          <label for="jenis_terminal2">Jenis Terminal</label>
          <input type="text" name="jenis_terminal" id="jenis_terminal2" class="form-control jenis_terminal" required>
        </div>
        <div class="col-sm-1 form-group form-message-dark">
          <label for="sto2">STO</label>
          <input type="text" name="sto" id="sto2" class="form-control" value="{{ $data->sto or '' }}" required>
        </div>
        <div class="col-sm-1 form-group form-message-dark">
          <label for="odc2">ODC</label>
          <input type="text" name="odc" id="odc2" class="form-control" value="{{ $data->odc or '' }}" required>
        </div>
        <div class="col-sm-2 form-group form-message-dark">
          <label for="lat">LAT</label>
          <input type="text" name="lat" id="lat" class="form-control" required>
        </div>
        <div class="col-sm-3 form-group form-message-dark">
          <label for="long">LONG</label>
          <input type="text" name="long" id="long" class="form-control" required>
        </div>
          <input type="hidden" name="jumlah_terminal" class="form-control" value="1">
        <input type="hidden" name="terminal" id="alpro">
        <div class="col-sm-2 form-group" style="margin-top:25px;">
          <button type="submit" class="btn btn-primary">Book</button>
          @if(count($booked))
          <button type="button" id="nextstep" class="btn btn-info pull-right"><i class="ion-soup-can"></i> Next Step</button>
          @endif
        </div>
        
      </div>
    </form>
  </div>
</div>

<form class="form-horizontal" method="post" id="formStep" enctype="multipart/form-data" action="/save_booking_odp/{{ Request::segment(2) }}">

</form>
<div id="map" style=" height: 400px;" class="col-sm-8"></div>
<div class="panel m-t-2 col-sm-12">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Label</th>
                <th>Terminal</th>
                <th>STO</th>
                <th>ODC</th>
                <th>Index</th>
                <th>Status</th>
                <th>Koordinat</th>
                <th>Tgl Order</th>
            </tr>
        </thead>
        <tbody>
            @foreach($booked as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ "$d->jenis_terminal-$d->sto-$d->odc/$d->index_terminal" }}</td>
                    <td>{{ $d->jenis_terminal }}</td>
                    <td>{{ $d->sto }}</td>
                    <td>{{ $d->odc }}</td>
                    <td>{{ $d->index_terminal }}</td>
                    <td>{{ $d->status }}</td>
                    <td>{{ $d->koordinat_kml }}</td>
                    <td>{{ $d->tgl_order }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        $('#formBookingOdp').pxValidate();
        $('#formBookingOdpSingle').pxValidate();
        $('#status_approval').select2({
            placeholder:'Select Status',
            data:[{'id':'3','text':'Approve'},{'id':'1','text':'Reject'},{'id':'99','text':'Cancel'}]
        });
        $('.jenis_terminal').select2({
            placeholder:'Select Status',
            data:[{'id':'ODP','text':'ODP = OPTICAL DISTRIBUTION POINT'},{'id':'GCL','text':'GCL = GPON CATU LANGSUNG'},{'id':'CL','text':'CL = CLOSURE FEEDER'},{'id':'ASPL','text':'ASPL = CLOSURE DISTRIBUSI'}]
        });
        $('#nextstep').click(function(){
            if (confirm("Lanjut ke Loker Berikutnya?") == true) {
              $('#formStep').submit();
            }
        });
    });
</script>
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
<script>
    $(function() {
        var data = <?= json_encode($odp); ?>;

        // var map = L.map('map').setView([-3.3172208,114.5591929], 12);
        // var tiles = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoid2FuZGl5OTkiLCJhIjoiY2wxazM1a2NtMHB6bjNkbjFmcnRqeG5vciJ9.X0UNxYJKqEBknpvgUp9ICg', {
        //     maxZoom: 18,
        //     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        //         'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        //     id: 'mapbox/streets-v11',
        //     tileSize: 512,
        //     zoomOffset: -1
        // }).addTo(map);

        var cities = L.layerGroup();

        var mLittleton = L.marker([39.61, -105.02]).bindPopup('This is Littleton, CO.').addTo(cities);
        var mDenver = L.marker([39.74, -104.99]).bindPopup('This is Denver, CO.').addTo(cities);
        var mAurora = L.marker([39.73, -104.8]).bindPopup('This is Aurora, CO.').addTo(cities);
        var mGolden = L.marker([39.77, -105.23]).bindPopup('This is Golden, CO.').addTo(cities);

        var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
        var mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoid2FuZGl5OTkiLCJhIjoiY2wxazM1a2NtMHB6bjNkbjFmcnRqeG5vciJ9.X0UNxYJKqEBknpvgUp9ICg';

        var grayscale = L.tileLayer(mbUrl, {id: 'mapbox/light-v9', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
        var streets = L.tileLayer(mbUrl, {id: 'mapbox/streets-v11', tileSize: 512, zoomOffset: -1, attribution: mbAttr});

        var map = L.map('map', {
            center: [39.73, -104.99],
            zoom: 10,
            layers: [streets]
        });

        var baseLayers = {
            'Grayscale': grayscale,
            'Streets': streets
        };

        var overlays = {
            'Cities': cities
        };

        var layerControl = L.control.layers(baseLayers, overlays).addTo(map);
        var satellite = L.tileLayer(mbUrl, {id: 'mapbox/satellite-v9', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
        layerControl.addBaseLayer(satellite, "Satellite");
        $('#folder_kml').change(function(){
            $('#jumlah_terminal').val(0);
            var i = $(this).val();
            if(data.Folder[i].Placemark.length){
                map.setView([data.Folder[i].Placemark[0].Point.coordinates.split(",")[1],data.Folder[i].Placemark[0].Point.coordinates.split(",")[0]], 16);
                $('#jumlah_terminal').val(data.Folder[i].Placemark.length);
                $.each(data.Folder[i].Placemark, function( index, value ) {
                    // alert(value.split(","));
                    L.marker([value.Point.coordinates.split(",")[1],value.Point.coordinates.split(",")[0]]).addTo(map)
                    .bindPopup(value.name);
                });

            }else{
                map.setView([data.Folder[i].Placemark.Point.coordinates.split(",")[1],data.Folder[i].Placemark.Point.coordinates.split(",")[0]], 16);
                $('#jumlah_terminal').val(1);
                L.marker([data.Folder[i].Placemark.Point.coordinates.split(",")[1],data.Folder[i].Placemark.Point.coordinates.split(",")[0]]).addTo(map)
                .bindPopup(data.Folder[i].Placemark.name);
            }
            $('#alpro').val(JSON.stringify(data.Folder[i].Placemark));
        });
    });
</script>
@endsection
