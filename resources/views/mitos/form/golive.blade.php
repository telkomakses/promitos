@extends('layout')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Golive</span>
</h1>
@endsection
@section('title', 'Golive')
@section('content')
<form method="post" id="form-register" enctype="multipart/form-data" action="/save_golive/{{ Request::segment(2) }}">
    <div class="row">
        <div class="col-sm-6 form-group form-message-dark">
          <label for="grid-input-16">ID SW</label>
          <input type="text" name="id_sw" id="id_sw" class="form-control" required/>
        </div>
        <div class="col-sm-6 form-group form-message-dark">
          <label for="grid-input-17">FILE SW</label>
          <input type="file" name="file_sw" id="file_sw" class="form-control" required>
        </div>
    </div>
    <div class="form-group">
        <div class="">
            <button type="submit" class="btn btn-primary pull-right"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
<div class="panel m-t-2 col-sm-12">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Label</th>
                <th>Terminal</th>
                <th>STO</th>
                <th>ODC</th>
                <th>Index</th>
                <th>Status</th>
                <th>Koordinat</th>
                <th>Tgl Order</th>
                <th>Durasi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($booked as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ "$d->jenis_terminal-$d->sto-$d->odc/$d->index_terminal" }}</td>
                    <td>{{ $d->jenis_terminal }}</td>
                    <td>{{ $d->sto }}</td>
                    <td>{{ $d->odc }}</td>
                    <td>{{ $d->index_terminal }}</td>
                    <td>{{ $d->status }}</td>
                    <td>{{ $d->koordinat_kml }}</td>
                    <td>{{ $d->tgl_order }}</td>
                    <td>{{ $d->duration }} Hari</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        // $('#upload_lop').pxFile();
        $('#status_approval').select2({
            placeholder:'Select Status',
            data:[{'id':'3','text':'Approve'},{'id':'1','text':'Reject'},{'id':'99','text':'Cancel'}]
        });
    });
</script>
@endsection
