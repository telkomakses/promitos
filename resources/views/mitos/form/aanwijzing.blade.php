@extends('layout')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Aanwijzing</span>
</h1>
@endsection
@section('title', 'Aanwijzing')
@section('content')
<form class="form-horizontal" method="post" id="form-register" enctype="multipart/form-data" action="/save_aanwijzing/{{ Request::segment(2) }}">
    <!-- <div class="form-group form-message-dark">
        <label for="status_approval" class="col-md-2 control-label">Status</label>
        <div class="col-md-9">
            <input type="text" name="status_approval" id="status_approval" class="form-control" required/>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="prioritas" class="col-md-2 control-label">Prioritas</label>
        <div class="col-md-9">
            <input type="text" name="prioritas" id="prioritas" class="form-control" required/>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="catatan" class="col-md-2 control-label">Catatan</label>
        <div class="col-md-9">
            <textarea name="catatan" id="catatan" class="form-control" required></textarea>
        </div>
    </div> -->
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        // $('#upload_lop').pxFile();
        $('#status_approval').select2({
            placeholder:'Select Status',
            data:[{'id':'3','text':'Approve'},{'id':'1','text':'Reject'},{'id':'99','text':'Cancel'}]
        });
    });
</script>
@endsection
